(function ($, Drupal) {
    Drupal.behaviors.injectHtmlBlock = {
      attach: function (context, settings) {
        // Function to inject HTML into a DIV
        function injectHTML(divId, htmlContent) {
          const targetDiv = document.getElementById(divId);
          if (targetDiv) {
            targetDiv.innerHTML = htmlContent;
          } else {
            //console.error(`No element found with ID: ${divId}`);
          }
        }
  
      // HTML block to inject
        const htmlBlockButton = `
            <div id="libchat_880228355d78843539a0f73a14ff9d0e"><!-- button --></div>
        `;

        const htmlBlockBot = `
            <div id="libchat_b0ba9131b28ab75723be605402e68c52fa5b9536d02946f1578f5c87ff0e95a4"><!-- chatbot --></div>
        `;

        // Inject into a DIV with ID 'targetDiv'
        injectHTML('askUs', htmlBlockButton); 
        // To make it work, add 
        // `<div id="askUs"><!-- libchat --></div>` 
        // to the page in a Block 
        // `UW Copy Text`
      }
    };
  })(jQuery, Drupal);
  