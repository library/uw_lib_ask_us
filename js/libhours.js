(function ($, Drupal) {
    Drupal.behaviors.libCalTodayHours = {
      attach: function (context, settings) {
        // Ensure the code runs only once per element.
        $(context).find("#s_lc_tdh_293_0").once('libCalTodayHours').each(function () {
          var s_lc_tdh_293_0 = new $.LibCalTodayHours($(this), { iid: 293, lid: 0 });
        });
      }
    };
  })(jQuery, Drupal);