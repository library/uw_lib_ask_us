Library Ask Us Module
---------------------

This module introduces some JavaScript required for the LibChat widget
used by the UW Library WCMS3.

It adds the Javascript files required in the Library's WCMS site,
to allow to ingest the Libchat button or Chatbot using Layout builder blocks.

E.g. `<div id="askUs"><!-- libchat--></div>`

To activate the module:
- Clone the repo into the respective /modules folder on the site
- Enable the module in the Admin

The source code is available at https://git.uwaterloo.ca/library/uw_lib_ask_us.

The module is maintained by:
- Israel Cefrin <ijcefrin@uwaterloo.ca>
